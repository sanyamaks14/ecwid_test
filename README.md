# Ecwid Test

## Ссылка на github pages:
[ecwid_test](https://ecwid-test-sanyamaks14.vercel.app/)

## Стэк:
- [vue-cli](https://cli.vuejs.org/)
- [vuex](https://vuex.vuejs.org/)
- [vuex-persist](https://www.npmjs.com/package/vuex-persist)
- [Typescript](https://www.typescriptlang.org/)

## Linter:
- [ESLint](https://eslint.org/)

## Инструкции по запуску:
- Скачать или склонировать репозитори
- Установить зависимости при помощи yarn - `yarn`
- Запустить проект - `yarn serve`
- Запустить сборку build - `yarn build`

## Deploy
 - Для быстрого деплоя проекта был использован [Vercel](https://vercel.com/guides/deploying-vuejs-to-vercel)
## Макет
- За основу был использован макет из [фигмы](https://www.figma.com/file/vZPqQE5jZtyH9t3wIPVLvM/IST.BIZ.UA-shop-(Copy)?node-id=54%3A1432). Релизован не в точности, поскольку не было требований.

## Дополнительно:
### Troubleshooting:

- На мой взгляд Vuex имеет определённые сложности с точки зрения типизации, а именно типы не передавались во вью компоненты. Поэтому была переписана типизация не включая модули: [ссылка](https://gitlab.com/sanyamaks14/ecwid_test/-/blob/develop/src/types/vuex.d.ts)

- У `<template>`'ов во Vue также имеются проблемы с типизацией пропсов. Родительский компонент видит пропсы дочернего компонента, как `any`. Поэтому вместо привычных для Vue `<template>` была использован `JSX`.

- Возникли проблемы с рендером SVG-иконок. Статьи на medium, dev.to или stackoverflow не привели к успеху. Поиск по `issues` на Github и проектам людей привёл к решению повысить версию c 16 до 17-beta vue-svg-loader: `"vue-svg-loader": "^0.17.0-beta.2"`.
