import { Getters } from './store.types';

const getters: Getters = {
  main: (state) => state.main,
  categoryAndProducts: (state) => state.categoryAndProducts,
  productInfo: (state) => state.productInfo,
  shoppingCartProducts: (state) => state.shoppingCartProducts,
  isProductBought: (state) => (productId) => !!state.shoppingCartProducts.find(
    (shoppingCartProduct) => shoppingCartProduct.id === productId,
  ),
};

export default getters;
