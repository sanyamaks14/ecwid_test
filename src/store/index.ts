import { InjectionKey } from 'vue';
import {
  createStore,
  CustomStore,
  useStore,
} from 'vuex';
import VuexPersist from 'vuex-persist';
import {
  ShoppingCartProducts,
  State,
  Getters,
  Mutations,
  Actions,
} from '@/store/store.types';
import actions from './store.actions';
import mutations from './store.mutations';
import getters from './store.getters';

const vueLocalStorage = new VuexPersist({
  key: 'STORAGE_KEY',
  storage: window.localStorage,
  reducer: (
    state: {shoppingCartProducts: ShoppingCartProducts},
  ) => ({ shoppingCartProducts: state.shoppingCartProducts }),

});

const mainState: State = {
  main: {
    categories: {
      items: [],
    },
    products: {
      items: [],
    },
    isLoading: false,
    errorMessage: null,
  },
  categoryAndProducts: {
    category: null,
    categoryProducts: [],
    isLoading: false,
    errorMessage: null,
  },
  productInfo: {
    product: null,
    isLoading: false,
    errorMessage: null,
  },
  shoppingCartProducts: [],
};

export const useCustomStore = (
  key: InjectionKey<unknown>,
) => useStore<State, Getters, Mutations, Actions>(key);

export const key: InjectionKey<CustomStore<State, Getters, Mutations, Actions>> = Symbol('state');

export const store = createStore<State, Getters, Mutations, Actions>({
  state: mainState,
  getters,
  mutations,
  actions,
  plugins: [vueLocalStorage.plugin],
});
