import { Mutations, MutationsTypes } from './store.types';

const mutations: Mutations = {
  [MutationsTypes.SET_MAIN_CATEGORIES](state, categories) {
    state.main = { ...state.main, categories };
  },
  [MutationsTypes.SET_MAIN_PRODUCTS](state, products) {
    state.main = { ...state.main, products };
  },
  [MutationsTypes.SET_MAIN_IS_LOADING](state, isLoading) {
    state.main = { ...state.main, isLoading };
  },
  [MutationsTypes.SET_MAIN_ERROR_MESSAGE](state, errorMessage) {
    state.main = { ...state.main, errorMessage };
  },
  [MutationsTypes.SET_CAP_CATEGORY](state, category) {
    state.categoryAndProducts = { ...state.categoryAndProducts, category };
  },
  [MutationsTypes.SET_CAP_CATEGORY_PRODUCTS](state, categoryProducts) {
    state.categoryAndProducts = {
      ...state.categoryAndProducts,
      categoryProducts: [...categoryProducts],
    };
  },
  [MutationsTypes.SET_CAP_IS_LOADING](state, isLoading) {
    state.categoryAndProducts = { ...state.categoryAndProducts, isLoading };
  },
  [MutationsTypes.SET_CAP_ERROR_MESSAGE](state, errorMessage) {
    state.categoryAndProducts = { ...state.categoryAndProducts, errorMessage };
  },
  [MutationsTypes.SET_PI_PRODUCT](state, product) {
    state.productInfo = { ...state.productInfo, product };
  },
  [MutationsTypes.SET_PI_IS_LOADING](state, isLoading) {
    state.productInfo = { ...state.productInfo, isLoading };
  },
  [MutationsTypes.SET_PI_ERROR_MESSAGE](state, errorMessage) {
    state.productInfo = { ...state.productInfo, errorMessage };
  },
  [MutationsTypes.ADD_SET_SHOPPING_CART_PRODUCT](state, shoppingCartProduct) {
    state.shoppingCartProducts = [...state.shoppingCartProducts, shoppingCartProduct];
  },
  [MutationsTypes.SET_SHOPPING_CART](state, shoppingCartProducts) {
    state.shoppingCartProducts = shoppingCartProducts;
  },
  [MutationsTypes.REMOVE_SET_SHOPPING_CART_PRODUCT](state, shoppingCartProductId) {
    const shoppingCartProduct = state.shoppingCartProducts.findIndex(
      (prod) => prod.id === shoppingCartProductId,
    );
    if (shoppingCartProduct > -1) {
      state.shoppingCartProducts = [
        ...state.shoppingCartProducts.slice(
          0,
          shoppingCartProduct,
        ),
        ...state.shoppingCartProducts.slice(shoppingCartProduct + 1),
      ];
    }
  },
};

export default mutations;
