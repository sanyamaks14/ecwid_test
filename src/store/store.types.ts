import { CustomActionContext } from 'vuex';

export type Product = {
  id: number;
  title: string;
  smallImage: string;
  priceText: string;
  price: number;
  description: string;
  image: string;
  images: {
    id: string;
    image160pxUrl: string;
    image400pxUrl: string;
    image800pxUrl: string;
    image1500pxUrl:string;
    imageOriginalUrl: string;
    isMain: boolean;
    orderBy: number;
  }[]
}

export type ShoppingCartProducts = Product[];

export type CategoryProducts = Product[];

export type Category = {
  name: string;
  productIds: number[];
}

export type CategoriesItem = {
    id: number;
    title: string;
    image: string;
  }

export type Categories = {
      items: CategoriesItem[];
}

export type Products = {
      items: Product[];
}

export type State = {
  main: {
    categories: Categories;
    products: Products;
    isLoading: boolean,
    errorMessage: string | null,
  }
  categoryAndProducts: {
    category: Category | null;
    categoryProducts: CategoryProducts | null;
    isLoading: boolean,
    errorMessage: string | null,
  },
  productInfo: {
    product: Product | null;
    isLoading: boolean,
    errorMessage: string | null,
  }
  shoppingCartProducts: ShoppingCartProducts;
}

export type Getters = {
  main: (state: State) => State['main'];
  categoryAndProducts: (state: State) => State['categoryAndProducts'];
  productInfo: (state: State) => State['productInfo'];
  shoppingCartProducts: (state: State) => ShoppingCartProducts;
  isProductBought: (state: State) => (productId: number) => boolean
}

export enum MutationsTypes {
  SET_MAIN_CATEGORIES = 'SET_CATEGORIES',
  SET_MAIN_PRODUCTS = 'SET_PRODUCTS',
  SET_MAIN_IS_LOADING = 'SET_MAIN_IS_LOADING',
  SET_MAIN_ERROR_MESSAGE = 'SET_MAIN_ERROR_MESSAGE',
  SET_CAP_CATEGORY = 'SET_CAP_CATEGORY',
  SET_CAP_CATEGORY_PRODUCTS = 'SET_CAP_CATEGORY_PRODUCTS',
  SET_CAP_IS_LOADING = 'SET_CAP_MAIN_IS_LOADING',
  SET_CAP_ERROR_MESSAGE = 'SET_CAP_MAIN_ERROR_MESSAGE',
  SET_PI_PRODUCT = 'SET_PI_PRODUCT',
  SET_PI_IS_LOADING = 'SET_PI_IS_LOADING',
  SET_PI_ERROR_MESSAGE = 'SET_PI_ERROR_MESSAGE',
  SET_SHOPPING_CART = 'SET_SHOPPING_CART',
  ADD_SET_SHOPPING_CART_PRODUCT = 'ADD_SET_SHOPPING_CART_PRODUCT',
  REMOVE_SET_SHOPPING_CART_PRODUCT = 'REMOVE_SET_SHOPPING_CART_PRODUCT',
  RESET_PRODUCT = 'RESET_PRODUCT',
}

export type Mutations = {
  [MutationsTypes.SET_MAIN_CATEGORIES](state: State, categories: Categories): void,
  [MutationsTypes.SET_MAIN_PRODUCTS](state: State, products: Products): void,
  [MutationsTypes.SET_MAIN_IS_LOADING](state: State, isLoading: boolean): void,
  [MutationsTypes.SET_MAIN_ERROR_MESSAGE](state: State, errorMessage: string | null): void,
  [MutationsTypes.SET_CAP_CATEGORY](state: State, products: Category | null): void,
  [MutationsTypes.SET_CAP_CATEGORY_PRODUCTS](
    state: State, categoryProducts: CategoryProducts
    ): void,
  [MutationsTypes.SET_CAP_IS_LOADING](state: State, isLoading: boolean): void,
  [MutationsTypes.SET_CAP_ERROR_MESSAGE](state: State, errorMessage: string | null): void,
  [MutationsTypes.SET_PI_PRODUCT](state: State, product: Product | null): void,
  [MutationsTypes.SET_PI_IS_LOADING](state: State, isLoading: boolean): void,
  [MutationsTypes.SET_PI_ERROR_MESSAGE](state: State, errorMessage: string | null): void,
  [MutationsTypes.SET_SHOPPING_CART](state: State, shoppingCartProducts: Product[]): void,
  [MutationsTypes.ADD_SET_SHOPPING_CART_PRODUCT](state: State, shoppingCartProduct: Product): void,
  [MutationsTypes.REMOVE_SET_SHOPPING_CART_PRODUCT](
    state: State, shoppingCartProductId: number
  ): void,
}

export enum ActionTypes {
  REQUEST_CATEGORIES_AND_PRODUCTS = 'REQUEST_CATEGORIES_AND_PRODUCTS',
  RESET_MAIN = 'RESET_MAIN',
  REQUEST_CATEGORY_AND_PRODUCTS = 'REQUEST_CATEGORY_AND_PRODUCTS',
  RESET_CAP = 'RESET_CAP',
  REQUEST_PRODUCT = 'REQUEST_PRODUCT',
  RESET_PRODUCT_INFO = 'RESET_PRODUCT_INFO',
  ADD_SHOPPING_CART_PRODUCT = 'ADD_SHOPPING_CART_PRODUCT',
  RESET_SHOPPING_CART = 'RESET_SHOPPING_CART',
}

export type Actions = {
  [ActionTypes.REQUEST_CATEGORIES_AND_PRODUCTS](
    context: CustomActionContext<State, State, Getters, Mutations, Actions>
  ): void,
  [ActionTypes.RESET_MAIN](
    context: CustomActionContext<State, State, Getters, Mutations, Actions>
  ): void,
  [ActionTypes.REQUEST_CATEGORY_AND_PRODUCTS](
      context: CustomActionContext<State, State, Getters, Mutations, Actions>,
      payload: {categoryId: number}
  ): void,
  [ActionTypes.RESET_CAP](
    context: CustomActionContext<State, State, Getters, Mutations, Actions>
  ): void,
  [ActionTypes.REQUEST_PRODUCT](
      context: CustomActionContext<State, State, Getters, Mutations, Actions>,
      payload: {productId: number}
  ): void,
  [ActionTypes.RESET_PRODUCT_INFO](
    context: CustomActionContext<State, State, Getters, Mutations, Actions>
  ): void,
  [ActionTypes.ADD_SHOPPING_CART_PRODUCT](
    context: CustomActionContext<State, State, Getters, Mutations, Actions>,
    payload: {productId: number}
  ): void,
  [ActionTypes.RESET_SHOPPING_CART](
    context: CustomActionContext<State, State, Getters, Mutations, Actions>,
    payload: {productId: number}
  ): void,
}
