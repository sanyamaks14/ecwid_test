import { PromiseCategoryProducts } from '@/API/api.types';
import {
  getCategories, getCategory, getProduct, getProducts,
} from '@/API/axios';
import CategoriesDTO from '@/dto/Categories.dto';
import ProductsDTO from '@/dto/Products.dto';
import CategoryDTO from '@/dto/Category.dto';
import CategoryProductsDTO from '@/dto/CategoryProducts.dto';
import ProductDTO from '@/dto/Product.dto';
import { Actions, ActionTypes, MutationsTypes } from './store.types';

const actions: Actions = {
  async [ActionTypes.REQUEST_CATEGORIES_AND_PRODUCTS]({ commit }) {
    try {
      commit(MutationsTypes.SET_MAIN_IS_LOADING, true);
      const categories = await getCategories();
      const products = await getProducts();
      commit(MutationsTypes.SET_MAIN_CATEGORIES, CategoriesDTO.apiToDomain(categories));
      commit(MutationsTypes.SET_MAIN_PRODUCTS, ProductsDTO.apiToDomain(products));
    } catch (e) {
      if (e instanceof Error) {
        commit(MutationsTypes.SET_MAIN_ERROR_MESSAGE, e.message);
      } else {
        commit(MutationsTypes.SET_MAIN_ERROR_MESSAGE, 'Error');
      }
      throw Error('error');
    } finally {
      commit(MutationsTypes.SET_MAIN_IS_LOADING, false);
    }
  },
  async [ActionTypes.RESET_MAIN]({ commit }) {
    commit(MutationsTypes.SET_MAIN_CATEGORIES, { items: [] });
    commit(MutationsTypes.SET_MAIN_PRODUCTS, { items: [] });
    commit(MutationsTypes.SET_MAIN_ERROR_MESSAGE, null);
    commit(MutationsTypes.SET_MAIN_IS_LOADING, false);
  },
  async [ActionTypes.REQUEST_CATEGORY_AND_PRODUCTS]({ commit }, { categoryId }) {
    try {
      commit(MutationsTypes.SET_CAP_IS_LOADING, true);
      const category = await getCategory(categoryId);
      const { productIds } = category;
      let products: PromiseCategoryProducts = [];
      await Promise.all(
        productIds.map(async (productId) => {
          try {
            const product = await getProduct(productId);
            products = [...products, product];
          } catch {
            throw Error('error');
          }
        }),
      );
      commit(MutationsTypes.SET_CAP_CATEGORY, CategoryDTO.apiToDomain(category));
      commit(MutationsTypes.SET_CAP_CATEGORY_PRODUCTS, CategoryProductsDTO.apiToDomain(products));
    } catch (e) {
      if (e instanceof Error) {
        commit(MutationsTypes.SET_CAP_ERROR_MESSAGE, e.message);
      } else {
        commit(MutationsTypes.SET_CAP_ERROR_MESSAGE, 'Error');
      }
      throw Error('error');
    } finally {
      commit(MutationsTypes.SET_CAP_IS_LOADING, false);
    }
  },
  async [ActionTypes.RESET_CAP]({ commit }) {
    commit(MutationsTypes.SET_CAP_CATEGORY, null);
    commit(MutationsTypes.SET_CAP_CATEGORY_PRODUCTS, []);
    commit(MutationsTypes.SET_CAP_ERROR_MESSAGE, null);
    commit(MutationsTypes.SET_CAP_IS_LOADING, false);
  },
  async [ActionTypes.REQUEST_PRODUCT]({ commit }, { productId }) {
    try {
      commit(MutationsTypes.SET_PI_IS_LOADING, true);
      const product = await getProduct(productId);
      commit(MutationsTypes.SET_PI_PRODUCT, ProductDTO.apiToDomain(product));
    } catch (e) {
      if (e instanceof Error) {
        commit(MutationsTypes.SET_PI_ERROR_MESSAGE, e.message);
      } else {
        commit(MutationsTypes.SET_PI_ERROR_MESSAGE, 'Error');
      }
      throw Error('error');
    } finally {
      commit(MutationsTypes.SET_PI_IS_LOADING, false);
    }
  },
  async [ActionTypes.ADD_SHOPPING_CART_PRODUCT]({ commit }, { productId }) {
    try {
      commit(MutationsTypes.SET_MAIN_IS_LOADING, true);
      commit(MutationsTypes.SET_CAP_IS_LOADING, true);
      commit(MutationsTypes.SET_PI_IS_LOADING, true);
      const product = await getProduct(productId);
      commit(MutationsTypes.ADD_SET_SHOPPING_CART_PRODUCT, ProductDTO.apiToDomain(product));
    } catch (e) {
      if (e instanceof Error) {
        commit(MutationsTypes.SET_MAIN_ERROR_MESSAGE, e.message);
        commit(MutationsTypes.SET_CAP_ERROR_MESSAGE, e.message);
        commit(MutationsTypes.SET_PI_ERROR_MESSAGE, e.message);
      } else {
        commit(MutationsTypes.SET_MAIN_ERROR_MESSAGE, 'Error');
        commit(MutationsTypes.SET_CAP_ERROR_MESSAGE, 'Error');
        commit(MutationsTypes.SET_PI_ERROR_MESSAGE, 'Error');
      }
      throw Error('error');
    } finally {
      commit(MutationsTypes.SET_MAIN_IS_LOADING, false);
      commit(MutationsTypes.SET_CAP_IS_LOADING, false);
      commit(MutationsTypes.SET_PI_IS_LOADING, false);
    }
  },
  async [ActionTypes.RESET_PRODUCT_INFO]({ commit }) {
    commit(MutationsTypes.SET_PI_PRODUCT, null);
    commit(MutationsTypes.SET_PI_ERROR_MESSAGE, null);
    commit(MutationsTypes.SET_PI_IS_LOADING, false);
  },
  [ActionTypes.RESET_SHOPPING_CART]({ commit }) {
    commit(MutationsTypes.SET_SHOPPING_CART, []);
  },
};

export default actions;
