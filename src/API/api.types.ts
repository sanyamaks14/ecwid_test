export type PromiseProduct = {
  id: number;
  name: string;
  smallThumbnailUrl: string;
  defaultDisplayedPriceFormatted: string;
  price: number;
  description: string;
  imageOriginalUrl: string;
  media: {
    images: {
      id: string;
      image160pxUrl: string;
      image400pxUrl: string;
      image800pxUrl: string;
      image1500pxUrl:string;
      imageOriginalUrl: string;
      isMain: boolean;
      orderBy: number;
    }[]
  }
}

export type PromiseCategoryProducts = PromiseProduct[]

export type PromiseCategory = {
  name: string;
  productIds: number[];
}

export type PromiseCategoriesItem = {
  id: number;
  name: string;
  thumbnailUrl: string;
}

export type PromiseCategories = {
    items: PromiseCategoriesItem[];
  }

export type PromiseProducts = {
    items: PromiseProduct[];
}
