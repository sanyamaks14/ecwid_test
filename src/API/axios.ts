import isAxiosError from '@/utils/typeGuards/isAxiosError';
import {
  PromiseCategories, PromiseCategory, PromiseProducts, PromiseProduct,
} from '@/API/api.types';
import axios from 'axios';

const STORE_ID = '58958138';

const axiosClient = axios.create({
  baseURL: `https://app.ecwid.com/api/v3/${STORE_ID}`,
});

const axiosParams = () => ({
  params: {
    token: 'public_7BxbJGWyDaZfSQqjVS5Ftr4jzXkS43UD',
  },
});

export const getCategories = async (): Promise<PromiseCategories> => {
  try {
    const { data } = await axiosClient.get<PromiseCategories>('categories', axiosParams());
    return data;
  } catch (error: unknown) {
    if (isAxiosError(error)) {
      if (error.response?.status === 403) {
        throw new Error('HTTP 403 Forbidden');
      }
      throw new Error('The request failed');
    }
    throw new Error('The request failed');
  }
};
export const getCategory = async (categoryId: number): Promise<PromiseCategory> => {
  try {
    const { data } = await axiosClient.get<PromiseCategory>(`categories/${categoryId}`, axiosParams());
    return data;
  } catch (error: unknown) {
    if (isAxiosError(error)) {
      if (error.response?.status === 403) {
        throw new Error('HTTP 403 Forbidden');
      }
      if (error.response?.status === 404) {
        throw new Error('Not Found');
      }
      throw new Error('The request failed');
    }
    throw new Error('The request failed');
  }
};
export const getProducts = async (): Promise<PromiseProducts> => {
  try {
    const { data } = await axiosClient.get<PromiseProducts>('products', axiosParams());
    return data;
  } catch {
    throw Error('The request failed');
  }
};

export const getProduct = async (productId: number): Promise<PromiseProduct> => {
  try {
    const { data } = await axiosClient.get<PromiseProduct>(`products/${productId}`, axiosParams());
    return data;
  } catch {
    throw Error('The request failed');
  }
};

export const awdwad = () => '';
