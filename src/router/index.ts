import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import ProductView from '@/views/ProductView';
import ShoppingCartView from '@/views/ShoppingCartView';
import HomeView from '@/views/HomeView';
import CategoryView from '@/views/CategoryView';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/product/:id',
    name: 'product',
    component: ProductView,
  },
  {
    path: '/category/:id',
    name: 'category',
    component: CategoryView,
  },
  {
    path: '/shopping-cart',
    name: 'shopping-cart',
    component: ShoppingCartView,
  },
  {
    path: '/',
    name: 'home',
    component: HomeView,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
