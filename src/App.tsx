import CommonHeader from '@/components/CommonHeader';
import styles from './App.style.scss';
import Footer from './components/Footer';

export default {
  components: {
    CommonHeader,
  },
  render() {
    return (
      <div class={styles.app}>
        <CommonHeader class={styles.header} />
        <main class={styles.main}>
          <router-view class={styles.page}/>
        </main>
        <Footer class={styles.footer}></Footer>
      </div>
    );
  },
};
