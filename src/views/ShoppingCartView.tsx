import { defineComponent } from 'vue';
import ProductsSection from '@/components/ProductsSection';
import { computed, ref } from '@vue/runtime-core';
import { useRouter } from 'vue-router';
import { ActionTypes, MutationsTypes } from '@/store/store.types';
import { key, useCustomStore } from '../store';
import styles from './ShoppingCartView.style.scss';

export default defineComponent({
  components: {
    ProductsSection,
  },
  setup() {
    const defaultOrderProcessedText = 'Заказ оформлен. Вы можете продолжить покупки в магазине.';
    const orderProcessedText = ref<string | null>(null);
    const store = useCustomStore(key);
    const router = useRouter();
    const shoppingCartProductsStore = computed(
      () => store.getters.shoppingCartProducts,
    );
    const shoppingCartProductsView = computed(
      () => shoppingCartProductsStore.value.map((product) => ({
        id: product.id,
        title: product.title,
        image: product.smallImage,
        price: product.priceText,
        isBought: true,
      })),
    );
    const orderPrice = computed(
      () => shoppingCartProductsStore.value.reduce((acc, product) => acc + product.price, 0),
    );

    const methods = {
      productClick: (productId: number) => {
        router.push(`/product/${productId}`);
      },
      buyProductClick: (productId: number) => {
        store.dispatch(ActionTypes.ADD_SHOPPING_CART_PRODUCT, { productId });
      },
      deleteProductClick: (productId: number) => {
        store.commit(MutationsTypes.REMOVE_SET_SHOPPING_CART_PRODUCT, productId);
      },
      order() {
        orderProcessedText.value = defaultOrderProcessedText;
        store.dispatch(ActionTypes.RESET_SHOPPING_CART);
      },
    };

    return {
      methods,
      shoppingCartProductsView,
      orderPrice,
      orderProcessedText,
    };
  },
  render() {
    return (
      <div class={styles.shoppingCartView}>
        <h2 class={styles.title}>Корзина</h2>
        {this.orderProcessedText
          && <div class={styles.orderProcessedText}>{this.orderProcessedText}</div>
        }
        {!this.orderProcessedText && (this.orderPrice ? <>
        <ProductsSection
          products={this.shoppingCartProductsView}
          productClick={(productId: number) => this.methods.productClick(productId)}
          buyProductClick={(productId: number) => this.methods.buyProductClick(productId)}
          deleteProductClick={(productId: number) => this.methods.deleteProductClick(productId)}
          />
          <div class={styles.orderInfo} onClick={this.methods.order}>
            <button class={styles.order}>Заказать</button>
            <p class={styles.orderPrice}>Сумма заказа: <b>{this.orderPrice}</b></p>
          </div>
        </>
          : <div class={styles.defaultTextContainer}>
            <p class={styles.defaultText}>Корзина пуста</p>
            </div>)}
      </div>
    );
  },
});
