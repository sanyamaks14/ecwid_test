import { defineComponent } from 'vue';
import ProductsSection from '@/components/ProductsSection';
import { computed, onMounted, Teleport } from '@vue/runtime-core';
import { useRoute, useRouter } from 'vue-router';
import Loader from '@/components/Loader';
import ErrorBlock from '@/components/ErrorBlock';
import { ActionTypes, MutationsTypes } from '@/store/store.types';
import { key, useCustomStore } from '../store';
import styles from './CategoryView.style.scss';

export default defineComponent({
  components: {
    ProductsSection,
  },
  setup() {
    const store = useCustomStore(key);
    const route = useRoute();
    const router = useRouter();
    const categoryAndProductsStore = computed(
      () => store.getters.categoryAndProducts,
    );
    const isProductBought = computed(() => store.getters.isProductBought);
    const categoryProductsView = computed(
      () => (categoryAndProductsStore.value.categoryProducts
        ? categoryAndProductsStore.value.categoryProducts.map((product) => ({
          id: product.id,
          title: product.title,
          image: product.smallImage,
          price: product.priceText,
          isBought: isProductBought.value(product.id),
        }))
        : []),
    );
    const categoryView = computed(() => categoryAndProductsStore.value.category);
    const isLoading = computed(() => categoryAndProductsStore.value.isLoading);
    const errorMessage = computed(() => categoryAndProductsStore.value.errorMessage);

    onMounted(() => {
      store.dispatch(ActionTypes.RESET_CAP);
      store.dispatch(
        ActionTypes.REQUEST_CATEGORY_AND_PRODUCTS,
        { categoryId: Number(route.params.id) },
      );
    });

    const methods = {
      productClick: (productId: number) => {
        router.push(`/product/${productId}`);
      },
      buyProductClick: (productId: number) => {
        store.dispatch(ActionTypes.ADD_SHOPPING_CART_PRODUCT, { productId });
      },
      deleteProductClick: (productId: number) => {
        store.commit(MutationsTypes.REMOVE_SET_SHOPPING_CART_PRODUCT, productId);
      },
    };

    return {
      methods,
      categoryView,
      categoryProductsView,
      isLoading,
      errorMessage,
    };
  },
  render() {
    return (
      <div class={styles.categoryView}>
      {this.isLoading && <Teleport to='#portal'><Loader /></Teleport>}
      {this.errorMessage && <ErrorBlock errorText={this.errorMessage}/>}
      {!this.errorMessage && <div>
        <h2 class={styles.title}>{this.categoryView?.name || ''}</h2>
        <ProductsSection
          products={this.categoryProductsView}
          productClick={(productId: number) => this.methods.productClick(productId)}
          buyProductClick={(productId: number) => this.methods.buyProductClick(productId)}
          deleteProductClick={(productId: number) => this.methods.deleteProductClick(productId)}
          />
          </div>
      }
      </div>
    );
  },
});
