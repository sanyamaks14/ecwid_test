import { defineComponent } from 'vue';
import {
  computed, onMounted, ref, Teleport,
} from '@vue/runtime-core';
import { useRoute, useRouter } from 'vue-router';
import BuyIcon from '@/assets/icons/buy-icon.svg';
import DeleteIcon from '@/assets/icons/delete-icon.svg';
import ErrorBlock from '@/components/ErrorBlock';
import Loader from '@/components/Loader';
import { ActionTypes, MutationsTypes } from '@/store/store.types';
import { key, useCustomStore } from '../store';
import styles from './ProductView.style.scss';

export default defineComponent({
  setup() {
    const store = useCustomStore(key);
    const router = useRouter();
    const route = useRoute();
    const currentImageId = ref<string>('0');
    const productStore = computed(() => store.getters.productInfo);
    const productView = computed(() => productStore.value.product);
    const isLoading = computed(() => productStore.value.isLoading);
    const errorMessage = computed(() => productStore.value.errorMessage);
    const isProductBought = computed(() => (productView?.value?.id
      ? store.getters.isProductBought(productView.value.id)
      : false));

    onMounted(() => {
      store.dispatch(ActionTypes.RESET_PRODUCT_INFO);
      store.dispatch(
        ActionTypes.REQUEST_PRODUCT,
        { productId: Number(route.params.id) },
      );
      currentImageId.value = productView?.value?.images
        ? productView.value.images.find((image) => image.isMain)?.id || '0'
        : '0';
    });

    const methods = {
      categoryClick: (categoryId: number) => {
        router.push(`/category/${categoryId}`);
      },
      buyProductClick: (productId: number) => {
        store.dispatch(ActionTypes.ADD_SHOPPING_CART_PRODUCT, { productId });
      },
      buyProductTextClick: (productId: number) => {
        if (!isProductBought.value) {
          store.dispatch(ActionTypes.ADD_SHOPPING_CART_PRODUCT, { productId });
        }
      },
      deleteProductClick: (productId: number) => {
        store.commit(MutationsTypes.REMOVE_SET_SHOPPING_CART_PRODUCT, productId);
      },
    };
    return {
      isProductBought,
      currentImageId,
      productView,
      methods,
      isLoading,
      errorMessage,
    };
  },
  render() {
    const { productView } = this;

    return (
      <div class={styles.productView}>
      {this.isLoading && <Teleport to='#portal'><Loader /></Teleport>}
      {this.errorMessage && <ErrorBlock errorText={this.errorMessage}/>}
      {!this.errorMessage
        && productView
        && <div class={styles.productInfo}><div class={styles.imagesContainer}>
          <div class={styles.toggleImages}>
            {productView.images.map((image) => <img
              class={styles.toggleImage}
              src={image.image160pxUrl}
              alt=""
              onClick={() => { this.currentImageId = image.id; } } />)}
          </div>
          <div class={styles.imageContainer}>
            <img
              class={styles.image}
              src={productView.images?.find(
                (image) => image.id === this.currentImageId,
              )?.imageOriginalUrl ?? ''}
              alt="" />
          </div>
        </div><div class={styles.productInfoContainer}>
            <h2 class={styles.title}>{productView.title}</h2>
            <p class={styles.price}>{productView.priceText}</p>
            <div class={styles.buyContainer}>
              <button
                class={[
                  styles.buyTextButton,
                  this.isProductBought && styles.butTextButtonDisabled,
                ]}
                onClick={() => this.methods.buyProductTextClick(productView.id)}
              >
                {!this.isProductBought ? 'Купить' : 'Куплено'}
              </button>
              {!this.isProductBought
                ? (<button
                  class={styles.buyIconButton}
                  onClick={() => this.methods.buyProductClick(productView.id)}
                >
                  <BuyIcon />
                </button>)
                : (<button
                  class={styles.deleteIconButton}
                  onClick={() => this.methods.deleteProductClick(productView.id)}
                >
                  <DeleteIcon />
                </button>)}
            </div>
            <div class={styles.descriptionContainer}>
              <h3 class={styles.descriptionTitle}>Описание</h3>
              <p class={styles.descriptionText} innerHTML={productView.description}></p>
            </div>
          </div></div>
      }
      </div>
    );
  },

});
