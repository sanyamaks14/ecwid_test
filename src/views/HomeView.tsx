import { defineComponent } from 'vue';
import CategoriesSection from '@/components/CategoriesSection';
import ProductsSection from '@/components/ProductsSection';
import {
  computed, onMounted, Teleport,
} from '@vue/runtime-core';
import { useRouter } from 'vue-router';
import { key, useCustomStore } from '@/store';
import { ActionTypes, MutationsTypes } from '@/store/store.types';
import Loader from '@/components/Loader';
import ErrorBlock from '@/components/ErrorBlock';
import styles from './HomeView.style.scss';

export default defineComponent({
  components: {
    CategoriesSection,
    ProductsSection,
  },
  setup() {
    const store = useCustomStore(key);
    const router = useRouter();
    const mainStore = computed(
      () => store.getters.main,
    );
    const isProductBought = computed(() => store.getters.isProductBought);
    const productsView = computed(
      () => mainStore.value.products.items.map((product) => ({
        id: product.id,
        title: product.title,
        image: product.smallImage,
        price: product.priceText,
        isBought: isProductBought.value(product.id),
      })),
    );
    const categoriesView = computed(
      () => store.getters.main.categories.items,
    );
    const isLoading = computed(() => mainStore.value.isLoading);
    const errorMessage = computed(() => mainStore.value.errorMessage);

    onMounted(() => {
      store.dispatch(ActionTypes.RESET_MAIN);
      store.dispatch(ActionTypes.REQUEST_CATEGORIES_AND_PRODUCTS);
    });

    const methods = {
      categoryClick: (categoryId: number) => {
        router.push(`/category/${categoryId}`);
      },
      productClick: (productId: number) => {
        router.push(`/product/${productId}`);
      },
      buyProductClick: (productId: number) => {
        store.dispatch(ActionTypes.ADD_SHOPPING_CART_PRODUCT, { productId });
      },
      deleteProductClick: (productId: number) => {
        store.commit(MutationsTypes.REMOVE_SET_SHOPPING_CART_PRODUCT, productId);
      },
    };

    return {
      errorMessage,
      isLoading,
      productsView,
      categoriesView,
      methods,
    };
  },
  render() {
    return (
      <div class={styles.home}>
        {this.isLoading && <Teleport to='#portal'><Loader /></Teleport>}
        {this.errorMessage && <ErrorBlock errorText={this.errorMessage}/>}
        {!this.errorMessage && <div class={styles.homeView}>
           {this?.categoriesView
           && <CategoriesSection
             categories={this.categoriesView}
             categoryClick={(categoryId: number) => this.methods.categoryClick(categoryId)}
           />}
           <ProductsSection
             products={this.productsView}
             productClick={(productId: number) => this.methods.productClick(productId)}
             buyProductClick={(productId: number) => this.methods.buyProductClick(productId)}
             deleteProductClick={(productId: number) => this.methods.deleteProductClick(productId)}
           />
        </div>}
      </div>
    );
  },

});
