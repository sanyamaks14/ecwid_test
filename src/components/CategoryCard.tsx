import { defineComponent, PropType } from 'vue';
import styles from './CategoryCard.style.scss';

export default defineComponent({
  props: {
    id: {
      type: Number,
      required: true,
    },
    image: {
      type: String,
      required: true,
    },
    title: {
      type: String,
      required: true,
    },
    onClick: {
      type: Function as PropType<(id: number) => void>,
      required: true,
    },
  },
  render() {
    return (
    <article
      class={styles.categoryCard}
      onClick={() => this.onClick(this.id)}
    >
        <div class="">
            <img
                class={styles.image}
                src={this.image ?? ''}
                alt=""
            />
        </div>
        <h3 class={styles.title}>{this.title}</h3>
    </article>
    );
  },
});
