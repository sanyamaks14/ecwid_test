import {
  computed,
  defineComponent,
  PropType,
} from 'vue';
import CategoryCard from '@/components/CategoryCard';
import { useRouter } from 'vue-router';
import styles from './CategoriesSection.style.scss';

export default defineComponent({
  components: {
    CategoryCard,
  },
  props: {
    categories: {
      default: Array,
      type: Array as PropType<{
        id: number,
        title: string,
        image: string,
      }[]>,
    },
    categoryClick: {
      type: Function as PropType<(categoryId: number) => void>,
      required: true,
    },
  },
  setup(props) {
    const router = useRouter();

    return {
      router, categoriesik: computed(() => props.categories),
    };
  },
  render() {
    return (
      <section class={styles.categories}>
        <h2 class={styles.title}>Категории</h2>
        <div class={styles.categoriesContainer}>
          {this.categories.map(
            (category) => <CategoryCard
                class={styles.categoryCard}
                key={category.id}
                id={1}
                image={category.image}
                title={category.title}
                onClick={() => this.categoryClick(category.id)}
            />,
          )}
        </div>
      </section>
    );
  },
});
