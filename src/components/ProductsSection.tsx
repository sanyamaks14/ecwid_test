import { defineComponent, PropType } from 'vue';
import ProductCard from '@/components/ProductCard';
import styles from './ProductsSection.style.scss';

export default defineComponent({
  components: {
    ProductCard,
  },
  methods: {
    showProduct(e: Event) {
      e.preventDefault();
    },
  },
  props: {
    products: {
      default: Array,
      type: Array as PropType<{
        id: number,
        title: string,
        price: string,
        image: string,
        isBought?: boolean,
      }[]>,
    },
    productClick: {
      type: Function as PropType<(productId: number) => void>,
      required: true,
    },
    buyProductClick: {
      type: Function as PropType<(productId: number) => void>,
      required: true,
    },
    deleteProductClick: {
      type: Function as PropType<(productId: number) => void>,
      required: true,
    },
  },
  render() {
    return (
      <section class={styles.products}>
        <h2 class={styles.title}>Товары</h2>
        <div class={styles.productsContainer}>
          {this.products.map((product) => (
            <ProductCard
              class={styles.productsCard}
              key={product.id}
              id={product.id}
              image={product.image}
              title={product.title}
              price={product.price}
              isBought={product.isBought}
              onClick={(productId: number) => this.productClick(productId)}
              buyProductClick={(productId: number) => this.buyProductClick(productId)}
              deleteProductClick={(productId: number) => this.deleteProductClick(productId)}
            />
          ))}
        </div>

      </section>
    );
  },
});
