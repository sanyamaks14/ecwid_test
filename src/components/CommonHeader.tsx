import { computed, defineComponent } from 'vue';
import Logo from '@/assets/icons/logo.svg';
import ShoppingCart from '@/assets/icons/shopping-cart.svg';
import { useRouter } from 'vue-router';
import { key, useCustomStore } from '@/store';
import styles from './CommonHeader.style.scss';

export default defineComponent({
  components: {
    Logo,
    ShoppingCart,
  },

  setup() {
    const router = useRouter();
    const store = useCustomStore(key);
    const shoppingCartProductsCount = computed(() => store.getters.shoppingCartProducts.length);
    const methods = {
      click: () => {
        router.push('/shopping-cart');
      },
    };
    return {
      methods,
      shoppingCartProductsCount,
    };
    //
  },
  render() {
    return (
      <header class={styles.header}>
        <Logo class={styles.logo} />
        <div class={styles.right}>
            <nav class={styles.nav}>
              <router-link class={styles.navItem} to="/">Магазин</router-link>
            </nav>
            <div class={styles.shoppingCartContainer} onClick={this.methods.click}>
                <ShoppingCart class={styles.shoppingCart} />
                { this.shoppingCartProductsCount
                  ? (<span class={styles.productsCount}>{this.shoppingCartProductsCount}</span>)
                  : ''
                }
            </div>
        </div>
      </header>
    );
  },
});
