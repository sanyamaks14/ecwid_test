import {
  defineComponent,
} from 'vue';
import styles from './Loader.style.scss';

export default defineComponent({

  render() {
    return (
        <div class={styles.loader}>
            <div class={styles.spinner}></div>
        </div>
    );
  },
});
