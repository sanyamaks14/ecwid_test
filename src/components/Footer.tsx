import {
  defineComponent,
} from 'vue';
import styles from './Footer.style.scss';

export default defineComponent({

  render() {
    return (
        <footer class={styles.footer}>
            <p class={styles.footerText}>Start Selling Online for Free</p>
        </footer>
    );
  },
});
