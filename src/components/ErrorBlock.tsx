import {
  defineComponent,
} from 'vue';
import styles from './ErrorBlock.style.scss';

export default defineComponent({
  props: {
    errorText: {
      type: String,
      required: true,
    },
  },
  render() {
    return (
        <div class={styles.errorBlock}>
            <p class={styles.errorText}>{this.errorText}</p>
        </div>
    );
  },
});
