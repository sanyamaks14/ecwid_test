import { defineComponent, PropType, ref } from 'vue';
import BuyIcon from '@/assets/icons/buy-icon.svg';
import DeleteIcon from '@/assets/icons/delete-icon.svg';
import styles from './ProductCard.style.scss';

export default defineComponent({
  props: {
    id: {
      type: Number,
      required: true,
    },
    title: {
      type: String,
      required: true,
    },
    image: {
      type: String,
      required: true,
    },
    price: {
      type: String,
      required: true,
    },
    onClick: {
      type: Function as PropType<(id: number) => void>,
      required: true,
    },
    buyProductClick: {
      type: Function as PropType<(id: number) => void>,
      required: true,
    },
    deleteProductClick: {
      type: Function as PropType<(id: number) => void>,
      required: true,
    },
    isBought: {
      type: Boolean,
      default: false,
    },
  },
  setup(props) {
    const isBuyButtonActive = ref(false);
    const methods = {
      clickBuyProduct: (e: Event, id: number) => {
        e.preventDefault();
        e.stopPropagation();

        props.buyProductClick(id);
      },
      deleteBuyProduct: (e: Event, id: number) => {
        e.preventDefault();
        e.stopPropagation();

        props.deleteProductClick(id);
      },
    };

    return {
      isBuyButtonActive,
      methods,
    };
  },
  render() {
    return (
      <article
        class={[
          styles.productCard,
          this.isBuyButtonActive ? styles.productCardActive : '',
        ]}
        onClick={() => this.onClick(this.id)}
      >
        <div class={styles.imageContainer}>
            <img
                class={styles.image}
                src={this.image}
                alt=""
            />
        </div>
        <div class={styles.infoContainer}>
            <h3 class={styles.title}>
                {this.title}
            </h3>
            <div class={styles.anotherInfo}>
                <p class={styles.price}>{this.price}</p>
                {!this.isBought
                  ? (<button
                    class={styles.buy}
                    onClick={(e) => this.methods.clickBuyProduct(e, this.id)}
                    onMouseenter={() => { this.isBuyButtonActive = true; }}
                    onMouseleave={() => { this.isBuyButtonActive = false; }}
                    >
                        <BuyIcon />
                    </button>)
                  : (<button
                    class={styles.delete}
                    onClick={(e) => this.methods.deleteBuyProduct(e, this.id)}
                    onMouseenter={() => { this.isBuyButtonActive = true; }}
                    onMouseleave={() => { this.isBuyButtonActive = false; }}
                    >
                        <DeleteIcon />
                    </button>)
                }
            </div>
        </div>
    </article>
    );
  },
});
