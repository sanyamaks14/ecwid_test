import * as vuex from 'vuex';

declare module 'vuex' {

  export interface CustomDispatch<A> {
    <K extends keyof A>(
      type: K, payload?: Parameters<A[K]>[1], options?: DispatchOptions
      ): Promise<any>;
    <P extends Payload>(payloadWithType: P, options?: DispatchOptions): Promise<any>;
  }

  export type CustomMutation<S> = (state: S, payload?: any) => any;

  export interface MutationTree<S> {
    [key: string]: CustomMutation<S>;
  }

  export interface CustomCommit<M> {
    <K extends keyof M>(type: K, payload?: Parameters<M[K]>[1], options?: CommitOptions): void;
    <P extends Payload>(payloadWithType: P, options?: CommitOptions): void;
  }

  export interface CustomActionContext<S, R, G, M, A> {
    dispatch: CustomDispatch<A>;
    commit: CustomCommit<M>;
    state: S;
    getters: {[K in keyof G]: ReturnType<G[K]>};
    rootState: R;
    rootGetters: any;
  }

  export type CustomActionHandler<S, R, G, M, A> = (
    this: Store<R>, injectee: CustomActionContext<S, R, G, M, A>, payload?: any
    ) => any;

  export interface CustomActionObject<S, R, G, M, A> {
    root?: boolean;
    handler: CustomActionHandler<S, R, G, M, A>;
  }
  export type CustomAction<S, R, G, M, A> =
  CustomActionHandler<S, R, G, M, A> | CustomActionObject<S, R, G, M, A>;

  export interface CustomActionTree<S, R, G, M, A> {
    [key: string]: CustomAction<S, R, G, M, A>;
  }

  type CustomGetter<S, R, G> =(state: S, getters: G, rootState: R, rootGetters: any) => G;

  declare interface CustomGetterTree<S, R, G> {
    [key: string]: CustomGetter<S, R, G>;
  }

  declare interface CustomStoreOptions<S, G, M, A> {
    state?: S | (() => S);
    getters?: G;
    actions?: A;
    mutations?: M;
    modules?: ModuleTree<S>;
    plugins?: Plugin<S>[];
    strict?: boolean;
    devtools?: boolean;
  }

  declare class CustomStore<S, G, M, A> {
    constructor(options: CustomStoreOptions<S, G, M, A>);

    readonly state: S;

    readonly getters: {[K in keyof G]: ReturnType<G[K]>};

    install(app: App, injectKey?: InjectionKey<Store<any>> | string): void;

    replaceState(state: S): void;

    dispatch: CustomDispatch<A>;

    commit: CustomCommit<M>;

    subscribe<P extends MutationPayload>(
      fn: (mutation: P, state: S
        ) => any, options?: SubscribeOptions): () => void;

    subscribeAction<P extends ActionPayload>(
      fn: SubscribeActionOptions<P, S>, options?: SubscribeOptions
      ): () => void;

    watch<T>(getter: (
      state: S, getters: any
      ) => T, cb: (value: T, oldValue: T) => void, options?: WatchOptions): () => void;

    registerModule<T>(path: string, module: Module<T, S>, options?: ModuleOptions): void;

    registerModule<T>(path: string[], module: Module<T, S>, options?: ModuleOptions): void;

    unregisterModule(path: string): void;

    unregisterModule(path: string[]): void;

    hasModule(path: string): boolean;

    hasModule(path: string[]): boolean;

    hotUpdate(options: {
      actions?: A;
      mutations?: MutationTree<S>;
      getters?: GetterTree<S, S, G>;
      modules?: ModuleTree<S>;
    }): void;
  }

  function createStore<S, G, M, A>(options: CustomStoreOptions<S, G, M, A>): CustomStore<S, G, M, A>

  function useStore<S = any, G, M, A>(
    injectKey?: InjectionKey<CustomStore<S, G, M, A>> | string
    ): CustomStore<S, G, M, A>;

}
