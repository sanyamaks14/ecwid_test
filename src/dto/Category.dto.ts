import { PromiseCategory } from '@/API/api.types';
import { Category } from '@/store/store.types';

const ApiToDomain = (input: PromiseCategory): Category => ({
  productIds: input.productIds,
  name: input.name,
});

export default {
  apiToDomain: ApiToDomain,
};
