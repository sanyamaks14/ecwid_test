import { PromiseCategories, PromiseCategoriesItem } from '@/API/api.types';
import { Categories, CategoriesItem } from '@/store/store.types';

const ApiToDomain = (input: PromiseCategories): Categories => ({
  items: input.items.map(
    (item:PromiseCategoriesItem): CategoriesItem => ({
      id: item.id,
      title: item.name,
      image: item.thumbnailUrl,
    }),
  ),
});

export default {
  apiToDomain: ApiToDomain,
};
