import { PromiseCategoryProducts } from '@/API/api.types';
import { CategoryProducts } from '@/store/store.types';
import ProductDTO from './Product.dto';

const ApiToDomain = (input: PromiseCategoryProducts): CategoryProducts => input.map(
  ProductDTO.apiToDomain,
);

export default {
  apiToDomain: ApiToDomain,
};
