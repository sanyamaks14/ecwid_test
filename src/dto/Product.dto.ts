import { PromiseProduct } from '@/API/api.types';
import { Product } from '@/store/store.types';

const ApiToDomain = (input: PromiseProduct): Product => ({
  id: input.id,
  title: input.name,
  smallImage: input.smallThumbnailUrl,
  priceText: input.defaultDisplayedPriceFormatted,
  price: input.price,
  description: input.description,
  images: input.media.images,
  image: input.imageOriginalUrl,
});

export default {
  apiToDomain: ApiToDomain,
};
