import { PromiseProducts } from '@/API/api.types';
import { Products } from '@/store/store.types';
import ProductDTO from './Product.dto';

const ApiToDomain = (input: PromiseProducts): Products => ({
  items: input.items.map(ProductDTO.apiToDomain),
});

export default {
  apiToDomain: ApiToDomain,
};
