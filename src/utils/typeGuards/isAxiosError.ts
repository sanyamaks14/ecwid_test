import { AxiosError } from 'axios';

export default function isAxiosError<T>(error: AxiosError | any): error is AxiosError<T> {
  return error && error.isAxiosError;
}
